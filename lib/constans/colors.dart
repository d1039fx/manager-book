import 'package:flutter/material.dart';

class ColorsData {
  final List<Color> degradee = [
    const Color(0xFFE1F5FE),
    const Color(0xFFE0F7FA),
    const Color(0xFFE0F7FA),
    const Color(0xFFE8F5E9),
    const Color(0xFFF1F8E9),
    const Color(0xFFF9FBE7),
  ];
}
