class Urls {
  final String urlApi = 'openlibrary.org';
  final String path = 'search.json';
  final String urlApiImage = 'covers.openlibrary.org';
  Uri urlSearchAuthor({String? nameAuthor}) => Uri.https(urlApi, path, {
        'author': '$nameAuthor',
        'fields':
            'title,edition_count,author_name,key,cover_edition_key,cover_i'
      });

  Uri urlSearchTitle({String? titleBook}) => Uri.https(urlApi, path, {
        'title': titleBook,
        'fields':
            'title,edition_count,author_name,key,cover_edition_key,cover_i'
      });

  Uri urlSearchImageBook({String? idImageBook}) =>
      Uri.https(urlApiImage, 'b/id/$idImageBook-L.jpg');

  Uri urlBookInfo({String? idBook}) => Uri.https(urlApi, 'books/$idBook.json');
}
