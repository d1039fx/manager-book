import 'package:hive/hive.dart';

part 'book_model.g.dart';

@HiveType(typeId: 1)
class BookModel {
  @HiveField(0)
  final String? key;

  @HiveField(1)
  final String? lendingEditionS;

  @HiveField(2)
  final String? title;

  @HiveField(3)
  final int? editCount;

  @HiveField(4)
  final List? authorName;

  @HiveField(5)
  final String? coverEditionKey;

  @HiveField(6)
  final int? coverI;

  BookModel(
      {this.key,
      this.title,
      this.editCount,
      this.lendingEditionS,
      this.authorName,
      this.coverEditionKey,
      this.coverI});

  Map<String, dynamic> toMap() => {
        "key": key,
        "title": title,
        "edition_count": editCount,
        "lending_edition_s": lendingEditionS,
        "author_name": authorName,
        "cover_edition_key": coverEditionKey,
        'cover_i': coverI
      };

  factory BookModel.fromJson(Map<String, dynamic> data) => BookModel(
      title: data['title'],
      key: data['key'],
      authorName: data['author_name'],
      coverEditionKey: data['cover_edition_key'],
      coverI: data['cover_i'],
      editCount: data['edition_count'],
      lendingEditionS: data['lending_edition_s']);
}
