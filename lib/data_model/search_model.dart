class SearchModel {
  final int? numFound, start;
  final bool? numFoundExact;
  final List<dynamic>? docs;

  SearchModel({this.numFound, this.start, this.numFoundExact, this.docs});

  Map<String, dynamic> toMap() => {
        'numFound': numFound,
        'start': start,
        'numFoundExact': numFoundExact,
        'docs': docs
      };

  factory SearchModel.fromJson(Map<String, dynamic> data) => SearchModel(
      docs: data['docs'],
      numFound: data['numFound'],
      numFoundExact: data['numFoundExact'],
      start: data['start']);
}
