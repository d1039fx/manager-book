class BookInfoModel {
  final List? publishers, lenguages, lcClassification, subjects, covers;
  final String? subTitle, title, fullTitle, publishDate;
  final int? numberOfPages, latestRevision, revision;

  BookInfoModel(
      {this.publishers,
      this.lenguages,
      this.lcClassification,
      this.subjects,
      this.covers,
      this.subTitle,
      this.title,
      this.fullTitle,
      this.publishDate,
      this.numberOfPages,
      this.latestRevision,
      this.revision});

  Map<String, dynamic> toMap() => {
  "publishers": publishers,
  "subtitle": subTitle,
  "title": title,
  "number_of_pages": numberOfPages,
  "languages": lenguages,
  "full_title": fullTitle,
  "lc_classifications": lcClassification,
  "publish_date": publishDate,
  "subjects": subjects,
  "covers": covers,
  "latest_revision": latestRevision,
  "revision": revision,
  };

  factory BookInfoModel.fromJson(Map<String, dynamic> data) => BookInfoModel(
    title: data['title'],
    covers: data['covers'],
    fullTitle: data['full_title'],
    latestRevision: data['latest_revision'],
    lcClassification: data['lc_classifications'],
    lenguages: data['languages'],
    numberOfPages: data['number_of_pages'],
    publishDate: data['publish_date'],
    publishers: data['publishers'],
    revision: data['revision'],
    subjects: data['subjects'],
    subTitle: data['subtitle'],
  );
}
