// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BookModelAdapter extends TypeAdapter<BookModel> {
  @override
  final int typeId = 1;

  @override
  BookModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BookModel(
      key: fields[0] as String?,
      title: fields[2] as String?,
      editCount: fields[3] as int?,
      lendingEditionS: fields[1] as String?,
      authorName: (fields[4] as List?)?.cast<dynamic>(),
      coverEditionKey: fields[5] as String?,
      coverI: fields[6] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, BookModel obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.key)
      ..writeByte(1)
      ..write(obj.lendingEditionS)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.editCount)
      ..writeByte(4)
      ..write(obj.authorName)
      ..writeByte(5)
      ..write(obj.coverEditionKey)
      ..writeByte(6)
      ..write(obj.coverI);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
