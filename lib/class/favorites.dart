import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:manager_books/data_model/book_model.dart';

class Favorites {
  final Box<BookModel> favorites = Hive.box('favorites');
  Icon favoriteIcon({String? key}) {
    if (favorites.containsKey(key)) {
      return const Icon(
        Icons.favorite_outlined,
        color: Colors.red,
      );
    } else {
      return const Icon(Icons.favorite_border_outlined);
    }
  }

  void favoriteSelection({BookModel? book}) {
    if (favorites.containsKey(book!.key)) {
      favorites.delete(book.key);
      return;
    } else {
      favorites.put(book.key, book);
    }
  }
}
