import 'package:get/get.dart';
import 'package:manager_books/class/db_class.dart';
import 'package:manager_books/class/favorites.dart';
import 'package:manager_books/data_model/book_model.dart';

class ListBookController extends GetxController with Favorites {
  RxList<BookModel> books = <BookModel>[].obs;
  RxBool changeSearchType = false.obs;
  RxBool changeFavoritesType = false.obs;
  RxBool stateInput = true.obs;
  RxBool stateData = false.obs;
  RxBool stateAnimation = false.obs;
  DataBase dataBase = DataBase();
  RxString inputSearchText = ''.obs;

  changeSearch() {
    books.clear();
    stateData.value = false;
    if (changeSearchType.isFalse) {
      changeSearchType.value = true;
      getListBookTitle(title: inputSearchText.value);
    } else {
      changeSearchType.value = false;
      getListBookAuthor(name: inputSearchText.value);
    }
  }

  void inputTextChange() {
    books.clear();
    if (changeSearchType.isTrue) {
      getListBookTitle(title: inputSearchText.value);
    } else {
      getListBookAuthor(name: inputSearchText.value);
    }
  }

  void changeFavorites() {
    if (changeFavoritesType.isFalse) {
      changeFavoritesType.value = true;
      stateInput.value = false;
      books.value = favorites.values.toList();
      stateAnimation.value = true;
    } else {
      stateData.value = false;
      changeFavoritesType.value = false;
      stateInput.value = true;
      inputTextChange();
      stateAnimation.value = false;
    }
  }

  void getListBookAuthor({String? name}) =>
      dataBase.getListBookAuthor(author: name).then((value) {
        List<BookModel> booksData =
            value.docs!.map<BookModel>((e) => BookModel.fromJson(e)).toList();
        books.value = booksData;
      }).whenComplete(() => stateData.value = true);
  void getListBookTitle({String? title}) =>
      dataBase.getListBookTitle(title: title).then((value) {
        List<BookModel> booksData =
            value.docs!.map<BookModel>((e) => BookModel.fromJson(e)).toList();
        books.value = booksData;
      }).whenComplete(() => stateData.value = true);
}
