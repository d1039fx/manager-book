import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:manager_books/constans/urls.dart';
import 'package:manager_books/data_model/book_info_model.dart';
import 'package:manager_books/data_model/search_model.dart';

class DataBase with Urls {
  final Urls urls = Urls();
  Future<SearchModel> getListBookAuthor({String? author}) async {
    return await http.get(urlSearchAuthor(nameAuthor: author)).then((value) {
      Map<String, dynamic> data = jsonDecode(value.body);
      return SearchModel.fromJson(data);
    });
  }

  Future<SearchModel> getListBookTitle({String? title}) async {
    return await http.get(urlSearchTitle(titleBook: title)).then((value) {
      Map<String, dynamic> data = jsonDecode(value.body);
      return SearchModel.fromJson(data);
    });
  }

  Future<BookInfoModel> getBookInfo({String? id}) async {
    return await http.get(urlBookInfo(idBook: id)).then((value) {
      Map<String, dynamic> data = jsonDecode(value.body);

      return BookInfoModel.fromJson(data);
    });
  }
}
