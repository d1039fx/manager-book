import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:manager_books/class/db_class.dart';
import 'package:manager_books/class/favorites.dart';
import 'package:manager_books/class/list_book_controller.dart';
import 'package:manager_books/constans/colors.dart';
import 'package:manager_books/data_model/book_model.dart';
import 'package:manager_books/page/info_book.dart';

class BooksListWidget extends StatelessWidget with ColorsData {
  final ListBookController listBookController = Get.put(ListBookController());
  final Favorites favorites = Favorites();
  final DataBase _dataBase = DataBase();

  BooksListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.transparent),
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                  colors: degradee,
                  end: Alignment.bottomCenter,
                  begin: Alignment.topCenter)),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: ListView.separated(
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 10,
                  );
                },
                itemCount: listBookController.books.length,
                itemBuilder: (context, index) {
                  BookModel bookData = listBookController.books[index];
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.8),
                      border: Border.all(color: Colors.transparent),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    margin: const EdgeInsets.symmetric(horizontal: 8),
                    child: ListTile(
                      onTap: () {
                        _dataBase.getBookInfo(id: bookData.coverEditionKey).then(
                            (value) =>
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => InfoBook(
                                          bookInfoModel: value,
                                          bookModel: bookData,
                                        ))));
                      },
                      leading: bookData.coverI == null
                          ? const Icon(
                              Icons.import_contacts_outlined,
                              size: 50,
                            )
                          : Hero(
                              tag: bookData.key!,
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    border: Border.all(color: Colors.grey[300]!),
                                    borderRadius: BorderRadius.circular(5)),
                                child: FadeInImage.assetNetwork(
                                    width: 50,
                                    placeholder: 'assets/img/book.png',
                                    fit: BoxFit.contain,
                                    image: _dataBase
                                        .urlSearchImageBook(
                                            idImageBook:
                                                bookData.coverI.toString())
                                        .toString()),
                              ),
                            ),
                      title: Text(bookData.title!),
                      subtitle: bookData.authorName == null
                          ? const Text('')
                          : Text(bookData.authorName!
                              .toString()
                              .replaceAll('[', '')
                              .replaceAll(']', '')),
                      trailing: IconButton(
                          onPressed: () {
                            favorites.favoriteSelection(book: bookData);
                          },
                          icon: favorites.favoriteIcon(key: bookData.key)),
                    ),
                  );
                }),
          ),
        ));
  }
}
