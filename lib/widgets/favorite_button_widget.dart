import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:manager_books/class/favorites.dart';
import 'package:manager_books/class/list_book_controller.dart';

class FavoriteButtonWidget extends StatelessWidget with Favorites {
  final ListBookController listBookController = Get.put(ListBookController());

  FavoriteButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.red),
          borderRadius: const BorderRadius.all(Radius.circular(50))),
      child: Obx(() => IconButton(
          tooltip: 'lista de favoritos',
          onPressed: listBookController.inputSearchText.isEmpty
              ? () =>  listBookController.changeFavorites()
              : !listBookController.stateData.value
                  ? null
                  : () {
                      listBookController.changeFavorites();
                    },
          icon: Icon(
            listBookController.changeFavoritesType.value
                ? Icons.favorite
                : Icons.favorite_border_outlined,
            color: Colors.red,
          ))),
    );
  }
}
