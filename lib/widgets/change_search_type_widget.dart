import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:manager_books/class/list_book_controller.dart';

class ChangeSearchTypeWidget extends StatelessWidget {
  final ListBookController listBookController = Get.put(ListBookController());

  ChangeSearchTypeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: CircleAvatar(
        backgroundColor: Colors.blue,
        child: Obx(() => IconButton(
              tooltip: 'Cambiar el tipo de busqueda',
              padding: const EdgeInsets.all(0),
              icon: listBookController.changeSearchType.isFalse
                  ? const Icon(Icons.person, color: Colors.white,)
                  : const Icon(
                      Icons.import_contacts_outlined
                , color: Colors.white,
                    ),
              onPressed: listBookController.changeFavoritesType.value
                  ? null
                  : listBookController.stateData.isFalse
                      ? null
                      : () {
                          listBookController.changeSearch();
                        },
            )),
      ),
    );
  }
}
