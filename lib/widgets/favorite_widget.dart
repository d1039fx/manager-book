import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:manager_books/class/favorites.dart';
import 'package:manager_books/class/list_book_controller.dart';
import 'package:manager_books/widgets/books_list_widget.dart';

class FavoriteWidget extends StatelessWidget with Favorites {
  final ListBookController listBookController = Get.put(ListBookController());

  FavoriteWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: favorites.listenable(),
        builder: (context, box, _) {
          return Obx(() => listBookController.inputSearchText.isEmpty
              ? BooksListWidget()
              : !listBookController.stateData.value
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : BooksListWidget());
        });
  }
}
