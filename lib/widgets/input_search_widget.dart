import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:manager_books/class/favorites.dart';
import 'package:manager_books/class/list_book_controller.dart';
import 'package:manager_books/widgets/change_search_type_widget.dart';
import 'package:manager_books/widgets/favorite_button_widget.dart';

class InputSearch extends StatefulWidget with Favorites {
  InputSearch({Key? key}) : super(key: key);

  @override
  State<InputSearch> createState() => _InputSearchState();
}

class _InputSearchState extends State<InputSearch> {
  final ListBookController listBookController = Get.put(ListBookController());

  final TextEditingController nameOrTitleSearch = TextEditingController();
  int timeAnimation = 750;

  List<String> inputSearchText = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.transparent),
          borderRadius: BorderRadius.circular(20),
        color: Colors.blue.withOpacity(0.1)
      ),
      padding: const EdgeInsets.only(top: 8, right: 8, bottom: 8),
      margin: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: LayoutBuilder(builder: (context, size) {
              return Stack(
                alignment: Alignment.center,
                children: [
                  Obx(() {
                    return AnimatedPositioned(
                      right: listBookController.stateAnimation.value
                          ? size.maxWidth
                          : 0,
                      curve: Curves.fastOutSlowIn,
                      duration: Duration(milliseconds: timeAnimation),
                      child: Row(
                        children: [
                          SizedBox(
                            width: size.maxWidth - 130,
                            child: TextFormField(
                              decoration: InputDecoration(
                                label: listBookController.changeSearchType.isFalse
                                    ? const Text('Autor')
                                    : const Text('Titulo'),
                                border: const OutlineInputBorder(),
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 8),
                              ),
                              controller: nameOrTitleSearch,
                              enabled: listBookController.stateInput.value,
                              onChanged: (valueChanged) {
                                inputSearchText.add(valueChanged);
                                listBookController.stateData.value = false;
                                Future.delayed(const Duration(seconds: 1))
                                    .then((value) {
                                  if (valueChanged.isEmpty) {
                                    listBookController.books.clear();
                                    inputSearchText.clear();
                                  } else {
                                    listBookController.inputSearchText.value =
                                        inputSearchText.last;
                                    listBookController.inputTextChange();
                                  }
                                });
                              },
                            ),
                          ),
                          ChangeSearchTypeWidget(),
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(50))),
                            child: IconButton(
                                tooltip: 'Limpiar busqueda',
                                onPressed:
                                    listBookController.changeFavoritesType.value
                                        ? null
                                        : !listBookController.stateData.value
                                            ? null
                                            : () {
                                                listBookController.books.clear();
                                                nameOrTitleSearch.clear();
                                                listBookController
                                                    .inputSearchText.value = '';
                                              },
                                icon: const Icon(Icons.close)),
                          )
                        ],
                      ),
                    );
                  }),
                  Obx(() {
                    return AnimatedPositioned(
                        top: listBookController.stateAnimation.value ? 15 : -50,
                        child: const Center(
                            child: Text(
                          'Favoritos',
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold, color: Colors.blue),
                        )),
                        curve: Curves.fastOutSlowIn,
                        duration: Duration(milliseconds: timeAnimation));
                  }),
                  SizedBox(
                    width: size.maxWidth,
                    height: 60,
                  ),
                ],
              );
            }),
          ),
          FavoriteButtonWidget()
        ],
      ),
    );
  }
}
