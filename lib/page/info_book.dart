import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:manager_books/constans/colors.dart';
import 'package:manager_books/constans/urls.dart';
import 'package:manager_books/data_model/book_info_model.dart';
import 'package:manager_books/data_model/book_model.dart';

class InfoBook extends StatelessWidget with Urls {
  final BookInfoModel? bookInfoModel;
  final BookModel? bookModel;
  final ColorsData colorsData = ColorsData();

  InfoBook({Key? key, this.bookInfoModel, this.bookModel}) : super(key: key);

  Container infoBookContainer({String? title, String? data, Color? color}) =>
      Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.transparent),
            borderRadius: BorderRadius.circular(20)),
        margin: const EdgeInsets.all(8),
        padding: const EdgeInsets.all(8),
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                title!,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
                child: data == null || data == 'null'
                    ? Text('No hay ${title.toLowerCase()}')
                    : Text(data))
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            iconTheme: const IconThemeData(color: Colors.blue),
            backgroundColor: Colors.transparent,
            expandedHeight: 300.0,
            pinned: true,
            elevation: 0,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                decoration: BoxDecoration(
                    color: Colors.indigo.withOpacity(0.1),
                    border: Border.all(color: Colors.transparent, width: 3),
                    borderRadius: BorderRadius.circular(20)),
                margin: const EdgeInsets.all(8),
                child: Hero(
                    tag: bookModel!.key!,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: bookModel!.coverI == null
                          ? Icon(
                              Icons.import_contacts_outlined,
                              size: 150,
                              color: Colors.grey[600],
                            )
                          : Image.network(urlSearchImageBook(
                                  idImageBook: bookModel!.coverI.toString())
                              .toString()),
                    )),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.withOpacity(0.1),
                      border: Border.all(color: Colors.transparent, width: 3),
                      borderRadius: BorderRadius.circular(20)),
                  margin: const EdgeInsets.all(8),
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 50,
                        child: Center(
                            child: FittedBox(
                          child: Text(
                            bookModel!.title!,
                            style: const TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                        )),
                      ),
                      bookInfoModel!.subTitle == null
                          ? const SizedBox()
                          : Center(
                              child: FittedBox(
                                  child: Text('${bookInfoModel!.subTitle}')))
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: colorsData.degradee,
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter),
                      border: Border.all(color: Colors.transparent, width: 3),
                      borderRadius: BorderRadius.circular(20)),
                  margin: const EdgeInsets.all(8),
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      infoBookContainer(
                        title: 'Numero de paginas',
                        data: bookInfoModel!.numberOfPages.toString(),
                      ),
                      infoBookContainer(
                          title: 'Fecha de publicacion',
                          data: bookInfoModel!.publishDate),
                      infoBookContainer(
                          title: 'Publicadores',
                          data: bookInfoModel!.publishers
                              .toString()
                              .replaceAll('[', '')
                              .replaceAll(']', '')),
                      infoBookContainer(
                          title: 'Lenguajes',
                          data: bookInfoModel!.lenguages
                              .toString()
                              .replaceAll('[{key: /languages/', '')
                              .replaceAll('}]', '')),
                      infoBookContainer(
                          data: bookInfoModel!.revision.toString(),
                          title: 'Revision'),
                      infoBookContainer(
                          data: bookInfoModel!.latestRevision.toString(),
                          title: 'Ultima revision'),
                      infoBookContainer(
                          data: bookInfoModel!.lcClassification
                              .toString()
                              .replaceAll('[', '')
                              .replaceAll(']', ''),
                          title: 'Clasificacion'),
                      infoBookContainer(
                          title: 'Temas relacionados',
                          data: bookInfoModel!.subjects
                              .toString()
                              .replaceAll('[', '')
                              .replaceAll(']', '')),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
